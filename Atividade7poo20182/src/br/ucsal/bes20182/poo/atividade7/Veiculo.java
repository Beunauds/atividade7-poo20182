package br.ucsal.bes20182.poo.atividade7;

public class Veiculo {
	private String placa;
	private Integer anofabricacao;
	private TipoVeiculoEnum tipo;
	
	public Veiculo(String placa, Integer anofabricacao, TipoVeiculoEnum tipo) {
		super();
		this.placa = placa;
		this.anofabricacao = anofabricacao;
		this.tipo = tipo;
	
		
	}

	public TipoVeiculoEnum getTipo() {
		return tipo;
	}

	public void setTipo(TipoVeiculoEnum tipo) {
		this.tipo = tipo;
	}

	public String getPlaca() {
		return placa;
	}

	public Integer getAnofabricacao() {
		return anofabricacao;
	}




}
