package br.ucsal.bes20182.poo.atividade7;

import java.util.ArrayList;
import java.util.List;

public class Contrato {
	private static Integer numerocontrato=0;
	private String nomecliente;
	private String enderecocliente;
	private List<Veiculo> veiculos= new ArrayList();
	private Double valortotal;

	public Contrato( String nomecliente, String enderecocliente) {
		this.nomecliente = nomecliente;
		this.enderecocliente = enderecocliente;
		numerocontrato++;

	}
	public String getNomecliente() {
		return nomecliente;
	}
	public String getEnderecocliente() {
		return enderecocliente;
	}
	public Double getValortotal() {
		return valortotal;
	}
	public static void setNumerocontrato(Integer numerocontrato) {
		Contrato.numerocontrato = numerocontrato;
	}


	public boolean adcionarVeiculos(Veiculo veiculo){
		if(veiculos.contains(veiculo.getPlaca())){
			return false;
		}else{
			veiculos.add(veiculo);
			return true;
		}

	}

	public boolean removerVeiculos(Veiculo veiculo){
		if(veiculos.contains(veiculo.getPlaca())){
			veiculos.remove(veiculo);
			return true;
		}else{
			return false;
		}
	}


	public List<Veiculo> consultarVeiculos(){
		return veiculos;


	}

	public List<Veiculo> consultarVeiculo(String tipo){
		if(veiculos.contains(tipo)){
			veiculos.equals(tipo);
			return  this.veiculos ;
		}else{
			return null;
		}

	}

	public Veiculo consultaVeiculo(String placa){
		if(veiculos.contains(placa)){

			return null;
		}else{
			return null;
		}

	}
}
